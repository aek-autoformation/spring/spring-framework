package com.autoformation.springfwk;

import com.autoformation.springfwk.basic.BinarySearchImpl;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class SpringfwkBasicApplication {

	// What are beans? -> @Component
	// What are the dependencies of the bean? -> @Autowired
	public static void main(String[] args) {

		try(AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringfwkBasicApplication.class)) {
			BinarySearchImpl binarySearch1 = context.getBean(BinarySearchImpl.class);
			BinarySearchImpl binarySearch2 = context.getBean(BinarySearchImpl.class);

			int result = binarySearch1.binarySearch(new int[]{12, 4, 6}, 3);
			System.out.println(result);
	
			// we have not same been
			System.out.println(binarySearch1);
			System.out.println(binarySearch2);
		} 
		

	}

}
