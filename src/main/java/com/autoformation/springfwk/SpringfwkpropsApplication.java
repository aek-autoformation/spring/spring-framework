package com.autoformation.springfwk;

import com.autoformation.springfwk.properties.SomeExternalService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;



@Configuration
@ComponentScan
@PropertySource("classpath:app.properties")
public class SpringfwkpropsApplication {
    public static void main(String[] args) {
        Logger logger = LoggerFactory.getLogger(SpringfwkpropsApplication.class);

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringfwkpropsApplication.class);

        SomeExternalService service = context.getBean(SomeExternalService.class);

        
        logger.info("{}",service.returnServiceURL());


    }
}
