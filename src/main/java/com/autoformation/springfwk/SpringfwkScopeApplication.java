package com.autoformation.springfwk;
import com.autoformation.springfwk.basic.BinarySearchImpl;
import com.autoformation.springfwk.scope.PersonDAO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan
public class SpringfwkScopeApplication {
    private static Logger LOGGER = LoggerFactory.getLogger(SpringfwkScopeApplication.class);
	// What are beans? -> @Component
	// What are the dependencies of the bean? -> @Autowired
	public static void main(String[] args) {
		// Where Spring will look for the beans
		ApplicationContext context = new AnnotationConfigApplicationContext(SpringfwkScopeApplication.class);


        /** I want a singleton PersonDAO 
         *  But different jdbc connection for each Person DAO Object
         *  We can do this by making jdbc connection a proxy
         */

        /**
         * Singleton of GoF makes sure that there is only one instance per JVM
         * Singleton In Spring makes sure there is one instance per application context
         */
		PersonDAO personDAO = context.getBean(PersonDAO.class);
        PersonDAO personDAO2 = context.getBean(PersonDAO.class);

        LOGGER.info("{}", personDAO);
        LOGGER.info("{}", personDAO.getJdbcConnection());

        LOGGER.info("{}", personDAO2);
        LOGGER.info("{}", personDAO2.getJdbcConnection());

	}

}
