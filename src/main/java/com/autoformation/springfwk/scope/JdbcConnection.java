package com.autoformation.springfwk.scope;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE, proxyMode = ScopedProxyMode.TARGET_CLASS)
//If we do not add prototype, well get samme connection for the Person Singleton
public class JdbcConnection {

    public JdbcConnection() {
        System.out.println("JDBC connection");
    }
    
}
