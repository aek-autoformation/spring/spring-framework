package com.autoformation.springfwk.basic;

public interface SortAlgorithm {
    public int[] sort(int[] numbers);
}
