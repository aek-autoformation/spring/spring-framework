package com.autoformation.springfwk.basic;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BinarySearchImpl {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("bubble")
    private SortAlgorithm sortAlgorithm;

    public int binarySearch(int[] numbers, int numberToSearchFor){
        sortAlgorithm.sort(numbers);
        return 3;
    }
    
    @PostConstruct
    public void postConstruct(){
        logger.info("postcontruct -----------------------");
    }

    //TODO the log is never printed!
    @PreDestroy
    public void preDestroy(){
        logger.info("preDestroy ---------------------------");
    }
}
