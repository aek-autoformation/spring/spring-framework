package com.autoformation.springfwk;
import com.autoformation.componentscan.ComponentPersonDAO;
import com.autoformation.springfwk.basic.BinarySearchImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan("com.autoformation.componentscan")
public class SpringfwkComponentScanApplication {
    private static Logger LOGGER = LoggerFactory.getLogger(SpringfwkComponentScanApplication.class);
	// What are beans? -> @Component
	// What are the dependencies of the bean? -> @Autowired
	public static void main(String[] args) {
		// Where Spring will look for the beans
		ApplicationContext context = new AnnotationConfigApplicationContext(SpringfwkComponentScanApplication.class);


        /** I want a singleton ComponentPersonDAO 
         *  But different jdbc connection for each Person DAO Object
         *  We can do this by making jdbc connection a proxy
         */

        /**
         * Singleton of GoF makes sure that there is only one instance per JVM
         * Singleton In Spring makes sure there is one instance per application context
         */
		ComponentPersonDAO componentPersonDAO = context.getBean(ComponentPersonDAO.class);
        ComponentPersonDAO componentPersonDAO2 = context.getBean(ComponentPersonDAO.class);

        LOGGER.info("{}", componentPersonDAO);
        LOGGER.info("{}", componentPersonDAO.getJdbcConnection());

        LOGGER.info("{}", componentPersonDAO2);
        LOGGER.info("{}", componentPersonDAO2.getJdbcConnection());

	}

}
