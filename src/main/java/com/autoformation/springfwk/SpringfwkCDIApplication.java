package com.autoformation.springfwk;
import com.autoformation.springfwk.basic.BinarySearchImpl;
import com.autoformation.springfwk.cdi.SomeCDIBusiness;
import com.autoformation.springfwk.scope.PersonDAO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan
public class SpringfwkCDIApplication {
    private static Logger LOGGER = LoggerFactory.getLogger(SpringfwkCDIApplication.class);
	// What are beans? -> @Component
	// What are the dependencies of the bean? -> @Autowired
	public static void main(String[] args) {
		// Where Spring will look for the beans
		ApplicationContext context = new AnnotationConfigApplicationContext(SpringfwkCDIApplication.class);


        SomeCDIBusiness someCDIBusiness = context.getBean(SomeCDIBusiness.class);

        LOGGER.info("{} -dao- {}",someCDIBusiness , someCDIBusiness.getSomeCDIDAO());
	}

}
