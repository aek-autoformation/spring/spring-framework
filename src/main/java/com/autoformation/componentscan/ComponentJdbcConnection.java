package com.autoformation.componentscan;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE, proxyMode = ScopedProxyMode.TARGET_CLASS)
//If we do not add prototype, well get samme connection for the Person Singleton
public class ComponentJdbcConnection {

    public ComponentJdbcConnection() {
        System.out.println("JDBC connection");
    }
    
}
